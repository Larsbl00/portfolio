// Writing this plain "pseudo boot sequence" with plain javascript is not fun.
// I miss my async/await statements :'(
//
// NOTE: This code was the designed to be functional, not pretty.
// If you want better looking code, checkout my github/gitlab.
// Feel free to critique my code there, feedback is always appreciated.

const FONT_SIZE = parseFloat(getComputedStyle(document.documentElement).fontSize)

const NUM_BOOT_ITEMS = Math.min(window.innerHeight / (1.75 * FONT_SIZE), 100)

/** Delay before prompts are logged. */
const BOOT_PROMPT_DELAY = 350
/** Maximum duration the boot sequence can last in ms */
const TOTAL_BOOT_DURATION = 1500

if (TOTAL_BOOT_DURATION < BOOT_PROMPT_DELAY) {
    throw new Error("Boot sequence duration is less than the delay")
}

/** Prompt spawn interval range. */
const BOOT_PROMPT_MAX = (TOTAL_BOOT_DURATION - BOOT_PROMPT_DELAY) / NUM_BOOT_ITEMS
const LOG_PROMPT_INTERVAL = { min: BOOT_PROMPT_MAX / 2, max: BOOT_PROMPT_MAX }


/** @note Keep max 21 (incl) chars wide. */
const BOOT_MESSAGES = [
    // Serious
    "BLE drivers found",
    "Cleared tempfs",
    "Console [tty0]",
    "Configuring keyboard",
    "Clearing caches",
    "Detected GPU",
    "Mounted filesystem",
    "Mounting NPU",
    "Starting systemdaemon",
    "New USB device found",
    "Fetched CPU info",
    "Found Wi-Fi network",
    "Found LoRA network",
    "Found RTC",
    "Initialized TPU",
    "I2C Controller created",
    "Load userspace",
    "Loading sentience 👽",
    "Setup kernel space",
    "Zigbee initialized",
    // Jokes
    "Answered live: 42",
    "Bread 👍",
    "Bending all elements",
    "Booted Eva Unit 01",
    "Boarding Bebop 🤠",
    "Cleaned codebase",
    "Connected Metaverse",
    "Casting Fireball 🔥",
    "Drank Water 💧",
    "Electric sheep found",
    "Entered code 451",
    "Entered the matrix",
    "Entering under dark ",
    "Finding spice",
    "Found boot quips",
    "Found source of evil",
    "Found snake in boot🐍",
    "Forking the eddies 🍴",
    "Following orders 🫡",
    "Hacked mainframe",
    "Hid stapler in jello",
    "Hid easter eggs 🥚",
    "Hekki Allmo 🔴🔵",
    "I Forgor 💀",
    "Installed Rust 🦀",
    "Init blockchain 🔗",
    "Initialized repo",
    "Init bruh moment",
    "Initializing Skynet",
    "Ignoring warnings",
    "Loaded sarcasm",
    "Merging Shai-Hulud",
    "Made coffee ☕",
    "Made friends 🥰",
    "\"Nice day\" init",
    "Not a git repository",
    "Praised to Ferris 🦀",
    "Pizza time 🍕",
    "Refueled tea 🍵",
    "Rolling initiative 🎲",
    "Running Voight-Kampff",
    "Running Arch BTW",
    "Running `cargo vroom`",
    "Rushed 🅱️",
    "Sent data to overlord",
    "Smiled and waved 🐧",
    "Skipping README.md",
    "Skibidi Toilet 📺🚽",
    "Uninstalled Java",
    "Untangled spaghetti",
    "Unstuck door 🚪",
    "Unpakcing joker 🃏🤡",
    "Used the force 🪄",
    "Vim (exited -1)",
    "Washed CPU",
    "Watching TV glow 📺",
    "🗿🗿🗿",
    "🧊🗻🚢",
    "❄️🚂🚄 One Train"
]

/**
 * Get a random element from weighted selection.
 * @template T Arbitrary type to select from.
 * @param {Record<T, number>} spec Mapping of options to weight.
 * @returns {function(): T} A function returning a random value with the distribution.
 */
function weightedRand(spec) {
    var i, j, table = []
    for (i in spec) {
        // The constant 10 below should be computed based on the
        // weights in the spec for a correct and optimal table size.
        // E.g. the spec {0:0.999, 1:0.001} will break this impl.
        for (j = 0; j < spec[i] * 100; j++) {
            table.push(i)
        }
    }
    return function () {
        return table[Math.floor(Math.random() * table.length)]
    }
}

/**
 * Get a random number in the interval $\[min, max\]$.
 * @param {Number} min Minimum value.
 * @param {Number} max Maximum value.
 * @returns A random number between `min` and `max`.
 */
function randomNumber(min, max) {
    return Math.random() * (max - min) + min
}

const randomStatus = weightedRand({ "ok": 0.80, "warn": 0.15, "error": 0.05 })

const main = document.getElementsByTagName("main")[0]
main.style.display = "none";


const terminalWindow = document.getElementsByClassName(
    "terminal-window"
)[0]


const logWindow = document.createElement("ul")
terminalWindow.appendChild(logWindow)

const initialText = document.createElement('li')
initialText.innerHTML = "Booting Portfoli.OS..."
logWindow.appendChild(initialText)


window.onload = () => {
    for (let i = 0; i < NUM_BOOT_ITEMS; i++) {
        setTimeout(() => {
            const log = document.createElement("li")
            log.setAttribute(
                "class",
                `status ${randomStatus()}`
            )
            log.innerHTML = BOOT_MESSAGES[
                Math.floor(Math.random() * BOOT_MESSAGES.length)
            ]
            logWindow.appendChild(log)
        },
            randomNumber(LOG_PROMPT_INTERVAL.min, LOG_PROMPT_INTERVAL.max) * i + BOOT_PROMPT_DELAY
        )
    }

    // Wait till all prompts are logged, then remove the log window.
    setTimeout(() => {
        logWindow.remove()
        main.style.display = null;
    },
        TOTAL_BOOT_DURATION,
    )
}


